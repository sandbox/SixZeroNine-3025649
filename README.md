##Description

Social Master module is a simple module that allows you to instantly push your new or updated content to Facebook.

Without this module you need to do this manually by going to Facebook debugger page and type or paste URL of the page and then click "Debug" and after that click on "Scrape new information"

This module is very useful for a website that publishes content more often because it reduces the time and author effort that is needed to properly show on Facebook.

##Real life situation

You have a website that publishes and edit existing content. You just published an article/product and manually or automatically share it to Facebook. 
Facebook sometimes need a little more time to scrape information for new article/product so there is a chance for example that image of article/product will not be shown so you will have to share article one more time.

After you published and shared article/product you just realized that you have a typo or maybe you want a different image or any og:meta information.
You will edit an article/product and then you will expect that new information is visible instantly on Facebook.

But that's not the case.

When you edit an already shared article/product Facebook will continue to display already cached data, so you need to manually scrape new information with Facebook debugger tool.
 
With this module this is happening automatically and you have no additional things to do except edit an article/product.

## Installing Social Master
##### Composer base approach is recommended!
1. Install the module as normal, note that there are two drupal modules dependencies and one more installed from composer.
2. Configure content types that you want to integrate with this module @ /admin/config/search/social_debugger/settings
3. You need to Authenticate with Facebook @ /admin/config/search/social_debugger/settings/facebook


## Authenticate Process
If you want to authenticate with Facebook you need to have Facebook application created: https://developers.facebook.com/apps

#####Enable Facbook Login API First! (On page: https://developers.facebook.com/apps/[ID]/dashboard/)

For step 3. You need to take informations from Facebook app:
1. App ID
2. App Secret
3. API Version (From page https://developers.facebook.com/apps/[ID]/settings/advanced/)

And you will need to enter allowed url for Authentification (From page: https://developers.facebook.com/apps/[ID]/fb-login/settings/) 
that you will get on /admin/config/search/social_debugger/settings/facebook configuration page 

When you will all the data you will click on "Save Configuration" button.

When page is reloaded you will see new Button "Log in with Facebook!"

Click on "Log in with Facebook!" button and you will be redirected to Facebook to confirm access from your website to facebook App.

When you confirm access you will be redirected back to configuration form and if everything is successful you will see: You are logged in with username: [YOUR USERNAME] 

##Credits:

Original module and idea: Strahinja Miljanovic (https://www.drupal.org/u/sixzeronine)
Current maintainers:

- Strahinja Miljanovic - https://www.drupal.org/u/sixzeronine
