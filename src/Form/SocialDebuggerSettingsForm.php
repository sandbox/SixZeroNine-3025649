<?php

namespace Drupal\social_master\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;

/**
 * Configure social master settings for this site.
 */
class SocialDebuggerSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_master_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'social_master.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('social_master.settings');

    // Get all entity types.
    $entity_types = NodeType::loadMultiple();

    $form['enabled_entity_types'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Entity types integration'),
      '#description' => $this->t('Enable entity types that you want to use for Social Debugger.'),
      '#tree' => TRUE,
    ];

    $form['enabled_social_platform_default_form'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Always enabled when creating/editing node'),
      '#description' => $this->t('Do you want to be always enabled when creating or editing node?'),
      '#tree' => TRUE,
    ];


    // Loop all loaded entity types.
    foreach ($entity_types as $entity) {
      $form['enabled_entity_types'][$entity->id()] = [
        '#type' => 'checkbox',
        '#title' => $this->t($entity->label()),
        '#default_value' => $config->get($entity->id()),
      ];
    }

    $form['enabled_social_platform_default_form']['facebook_default_enabled'] = [
      '#type' => 'checkbox',
      '#title' => 'Always keep checked Facebook on node add/edit forms',
      '#default_value' => $config->get('facebook_default_enabled'),
    ];
//    $form['enabled_social_platform_default_form']['twitter_default_enabled'] = [
//      '#type' => 'checkbox',
//      '#title' => 'Always keep checked Twitter on node add/edit forms',
//      '#default_value' => $config->get('twitter_default_enabled'),
//    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get form config.
    $config = $this->config('social_master.settings');

    $form_state->cleanValues();

    // Get and set form values.
    foreach ($form_state->getValues() as $data) {
      foreach ($data as $key => $value) {
        $config->set($key, $value);
      }
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }
}