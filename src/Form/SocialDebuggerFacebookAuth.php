<?php

namespace Drupal\social_master\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Facebook;
use Facebook\Exceptions;

/**
 * Configure social master Facebook auth for this site.
 */
class SocialDebuggerFacebookAuth extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_master_facebook_auth_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'social_master_facebook_auth.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();

    $config = $this->config('social_master_facebook_auth.settings');

    $app_id = $config->get('app_id');
    $app_secret = $config->get('app_secret');
    $graph_version = $config->get('graph_version');
    $redirect_uri = $config->get('redirect_uri');

    if (isset($app_id) && isset($app_secret) && isset($graph_version) && isset($redirect_uri)) {
      if (!empty($app_id) && !empty($app_secret) && !empty($graph_version) && !empty($redirect_uri)) {

        $fb = new Facebook\Facebook([
          'app_id' => $config->get('app_id'),
          'app_secret' => $config->get('app_secret'),
          'default_graph_version' => $config->get('graph_version'),
          'scope' => 'manage_pages, pages_show_list'
        ]);
        $helper = $fb->getRedirectLoginHelper();
        try {
          if (isset($_SESSION['facebook_access_token'])) {
            $accessToken = $_SESSION['facebook_access_token'];
          } else {
            $accessToken = $helper->getAccessToken();
          }
        } catch (Exceptions\FacebookResponseException $e) {

          // When Graph returns an error
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch (Exceptions\FacebookSDKException $e) {

          // When validation fails or other local issues
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }

        if (isset($accessToken)) {
          if (isset($_SESSION['facebook_access_token'])) {
            $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
          } else {
            // getting short-lived access token
            $_SESSION['facebook_access_token'] = (string)$accessToken;

            // OAuth 2.0 client handler
            $oAuth2Client = $fb->getOAuth2Client();

            // Exchanges a short-lived access token for a long-lived one
            $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
            $_SESSION['facebook_access_token'] = (string)$longLivedAccessToken;

            // setting default access token to be used in script
            $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
          }

          // redirect the user to the profile page if it has "code" GET variable

          if (isset($_GET['code'])) {
            header('Location: ' . $redirect_uri);
          }

          // getting basic info about user
          try {
            $profile_request = $fb->get('/me?fields=name,first_name,last_name,email');
            $requestPicture = $fb->get('/me/picture?redirect=false&height=200'); //getting user picture
            $picture = $requestPicture->getGraphUser();
            $profile = $profile_request->getGraphUser();
            $fbid = $profile->getProperty('id'); // To Get Facebook ID
            $fbfullname = $profile->getProperty('name'); // To Get Facebook full name
//            $fbemail = $profile->getProperty('email'); //  To Get Facebook email
            $fbpic = "<img src='" . $picture['url'] . "' class='img-rounded'/>";

            // save the user nformation in session variable

            $_SESSION['fb_id'] = $fbid . '</br>';
            $_SESSION['fb_name'] = $fbfullname . '</br>';
//            $_SESSION['fb_email'] = $fbemail . '</br>';
            $_SESSION['fb_pic'] = $fbpic . '</br>';
          } catch (Exceptions\FacebookResponseException $e) {

            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            session_destroy();

            // redirecting user back to app login page
            header("Location: ./");
            exit;
          } catch (Exceptions\FacebookSDKException $e) {

            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
          }
        } else {

          // replace your website URL same as added in the developers.Facebook.com/apps e.g. if you used http instead of https and you used
          $loginUrl = $helper->getLoginUrl($redirect_uri);

          $form['facebook_login'] = [
            '#type' => 'details',
            '#open' => TRUE,
            '#title' => $this->t('Click to login'),
            '#description' => $this->t('Now you can <div class="button button--primary facebook-login"><a href="' . $loginUrl . '">Log in with Facebook!</a></div>'),
            '#tree' => TRUE,
          ];

//          $form['facebook_login']['logged_in_status'] = [
//            '#markup' => '<div class="button button--primary facebook-login"><a href="' . $loginUrl . '">Log in with Facebook!</a></div>',
//            '#allowed_tags' => ['div', 'a'],
//          ];
        }
      }
    }

    $form['facebook_data'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Facebook data'),
      '#description' => $this->t('Enter valid Facebook data so you can get login button.'),
      '#tree' => TRUE,
    ];

    $form['facebook_data']['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#default_value' => $config->get('app_id'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
    ];

    $form['facebook_data']['app_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App Secret'),
      '#default_value' => $config->get('app_secret'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
    ];

    $form['facebook_data']['graph_version'] = [
      '#type' => 'select',
      '#title' => $this->t('Graph Version'),
      '#default_value' => $config->get('graph_version'),
      '#options' => [
        'v2.9' => 'v2.9',
        'v2.10' => 'v2.10',
        'v2.11' => 'v2.11',
        'v2.12' => 'v2.12',
        'v3.0' => 'v3.0',
        'v3.1' => 'v3.1',
        'v3.2' => 'v3.2',
        'v3.3' => 'v3.3',
      ],
      '#required' => TRUE,
    ];

    $form['facebook_data']['redirect_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect Uri'),
      '#default_value' => $config->get('redirect_uri'),
      '#description' => 'Copy this URL to Facebook app: </br> ' . \Drupal::request()->getSchemeAndHttpHost() . '/admin/config/search/social-master/settings/facebook',
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
    ];

    $form['text']['#markup'] = t("If you don't know how to get required data above, follow this instructions.</br>
    1. Go to the page: <a href='https://developers.facebook.com/apps/' target='_blank'> Facebook Apps </a></br>
    2. Click on Add new app if you don't have one already. If you do have app then click on it.</br>
    3. When you have your app created click on it and on the left side menu you will see PRODUCTS+ menu, click on the +</br>
    4. Now you will see list of Facebook products, and you need to click on Facebook Login (Set Up)</br>
    5. Chose WWW</br>
    6. Enter your site URL, click Save, click Continue, click Next, click Next, click Next and your done with this step</br>
    7. On the top of the page you will see APP ID: 123456789 (Example numbers), copy this numbers and paste it here in field App ID</br>
    8. On Facebook App page click on the Settings -> Basic and you will see App Secret. Click Show button after ****** Modal will show up to ask you for your Facebook password, enter password here</br>
    9. Copy App Secret key into form here on your site in field App Secret</br>
    10. On Facebook app page click on the Settings -> Advanced you will see Upgrade API Version. Here you can select version that you want - but make sure that version on this form is the same version as on Facebook app page.</br>
    11. On Facebook app page click on Facebook Login below PRODUCTS, and then click Settings.</br>
    12. Make sure that Yes option is on settings for Client OAuth Login, Web OAuth Login, Embedded Browser OAuth Login.
    13. Last step is the Valid OAuth Redirect URIs from page in prevous step. Enter your website domain. </br> Example: www.yourwebsite.com/admin/config/search/social-master/settings/facebook");

    $form['text']['#type'] = 'item';

    if (isset($accessToken) && $accessToken != NULL) {
      $messenger->addMessage(t('You are logged in with username: ' . $fbfullname), $messenger::TYPE_STATUS);
    } else {
      $messenger->addMessage(t('You are NOT logged in.'), $messenger::TYPE_ERROR);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('social_master_facebook_auth.settings');

    $form_state->cleanValues();

    foreach ($form_state->getValues() as $data) {
      foreach ($data as $key => $value) {
        $config->set($key, $value);
      }
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }
}

/**
 * @Todo Create Validation
 */
