<?php

namespace Drupal\social_master\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Facebook;

/**
 * Configure social master Facebook Page config for this site.
 */
class SocialMasterFacebookPage extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_master_facebook_page_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'social_master_facebook_page.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('social_master_facebook_auth.settings');

    $fb = new Facebook\Facebook([
      'app_id' => $config->get('app_id'),
      'app_secret' => $config->get('app_secret'),
      'default_graph_version' => $config->get('graph_version'),
    ]);

    $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    $pages = $fb->get('/me/accounts');
    $fb->get('/149108037646', $_SESSION['facebook_access_token']);
    $pages->getDecodedBody();

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('social_master_facebook_auth.settings');
    $form_state->cleanValues();

    foreach ($form_state->getValues() as $data) {
      foreach ($data as $key => $value) {
        $config->set($key, $value);
      }
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }
}

/**
 * @Todo Create Validation
 */
